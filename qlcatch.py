# -*- coding: utf-8 -*-
#
# @PROJECT       : `qlchatVideoDownloader`
# 
# @FILE          : `qlcatch.py`
# @CREATION TIME : `2020-01-09 12:41`
# @SCRIPT EDITOR : `PyCharm`
#
# @AUTHOR        : `Yuzaihhhh`
#
import sys
import time
import json
import os

import m3u8
import requests
from requests import utils


def std_input():
    script_name, *params = sys.argv
    input_data = {}
    counter = 0
    for param in params:
        if param == '--help':
            print("""python qlcatch.py --cookies /path/to/cookies.json""")
        elif param == '--cookies':
            input_data['cookies'] = params[counter+1]
        counter += 1

    return input_data

def load_cookies(path: str=None):
    if not path:
        path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), 'cookies.json'
        )
    with open(path, 'r+') as cookies_file:
        cookies_text = cookies_file.read()
        cookies_dict = json.loads(cookies_text)

    return cookies_dict


def main():

    STD_INPUT_DATA = std_input()

    URL_CATCH_SESSION = requests.session()

    URL_CATCH_SESSION.headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:71.0) Gecko/20100101 Firefox/71.0',
        'Accept': '*/*',
        'Accept-Language': 'zh,zh-CN;q=0.9,zh-HK;q=0.7,zh-TW;q=0.6,zh-SG;q=0.4,en;q=0.3,en-US;q=0.1',
        'Accept-Encoding': 'gzip, deflate, br',
        'DNT': '1',
        'Connection': 'keep-alive',
    }

    URL_CATCH_SESSION.cookies = utils.cookiejar_from_dict(
        load_cookies(STD_INPUT_DATA.get('cookies', ''))
    )

    JSON_RESP = URL_CATCH_SESSION.get(
        'https://m.qlchat.com/api/wechat/channel/topic-list?channelId=2000006817141754&liveId=2000005950132562&clientType=weixin&pageNum=1&pageSize=99999'
    )

    def fetch_video_info(rep):
        counter = 1
        info_ls = []
        for topic in rep.json()['data']['topicList']:
            info_ls.append(
                (topic['id'], '. '.join((str(counter), topic['topic'])))
            )
            counter += 1
        return info_ls

    # POST {"topicId":"2000007196294323"} 来获得流文件
    def fetch_video_url(video_info_ls):
        video_m3u8_ls = []
        for vid, _ in video_info_ls:
            json_rep = URL_CATCH_SESSION.post(
                'https://m.qlchat.com/api/wechat/topic/getMediaActualUrl',
                json=dict(topicId=vid)
            ).json()
            temp = json_rep.get('data').get('video')
            if temp:
                video_m3u8_ls.append(temp[0].get('playUrl'))
            else:
                video_m3u8_ls.append(json_rep.get('data').get('audio').get('playUrl'))
        return video_m3u8_ls

    def video_saver(video_cont, filename, file_extend):
        with open('videos/' + '.'.join((filename, file_extend,)), 'wb+') as video_fl:
            video_fl.write(video_cont)

    def audio_saver(video_cont, filename, file_extend):
        video_saver(video_cont, filename, file_extend)

    def video_downloader(video_url_ls, video_id_ls):
        counter = 0
        for url in video_url_ls:
            if counter < 11:
                counter += 1
                continue
            try:
                video_segments = m3u8.load(url).segments
            except:
                print('downloading {}...'.format(video_id_ls[counter][-1]))
                audio_content = requests.get(url, headers=URL_CATCH_SESSION.headers).content
                print('saving {}...'.format(video_id_ls[counter]))
                video_saver(audio_content, video_id_ls[counter][-1], 'mp3')
                print('finished !')
                print('sleeping...')
                counter += 1
                time.sleep(30)
                continue
            video_ts = b''
            print('downloading {}...'.format(video_id_ls[counter][-1]))
            for video_absurl in video_segments:
                video_ts += requests.get(video_absurl.absolute_uri, headers=URL_CATCH_SESSION.headers).content
            print('saving {}...'.format(video_id_ls[counter]))
            video_saver(video_ts, video_id_ls[counter][-1], 'ts')
            counter += 1
            print('finished !')
            print('sleeping...')
            time.sleep(30)

    video_id_ls = fetch_video_info(JSON_RESP)
    print('fetching video info...')
    print('finished !')
    video_url_ls = fetch_video_url(video_id_ls)
    print('fetching video url...')
    print('finished !')
    video_downloader(video_url_ls, video_id_ls)


if __name__ == '__main__':
    main()

