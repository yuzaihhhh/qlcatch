# 说明

qlcatch 是一款用于下载 `m.qlchat.com` 这个网站课程的小程序。

**环境**

- `python3`

**依赖**

- `m3u8`

- `requests`

**使用**

*\(可能已经失效\)*

使用时需要添加cookies。仅支持json格式（因为方便）。默认会读取 `qlcatch.py` 同级目录下的 `cookies.json`

```bash
python qlctch.py --cookies /path/to/cookies.json
```
